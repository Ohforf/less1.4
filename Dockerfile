FROM debian:9 AS builder
RUN apt update && apt install -y wget linux-kernel-headers build-essential libpcre++-dev libssl-dev zlib1g zlib1g-dev
RUN cd && wget https://nginx.org/download/nginx-1.20.2.tar.gz && tar xvfz nginx-1.20.2.tar.gz && cd nginx-1.20.2 && ./configure && make && make install

FROM debian:9
RUN apt update && apt install -y curl
WORKDIR /usr/local/nginx/sbin
COPY --from=builder /usr/local/nginx/sbin/nginx .
RUN chmod +x nginx && mkdir ../conf ../logs /data && touch ../logs/error.log
CMD ["./nginx", "-g", "daemon off;"]